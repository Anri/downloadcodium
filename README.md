# Téléchargement et utilisation

- Lancez cette commande :
  Va télécharger le script, et le lancer tout seul, en se supprimant à la fin de l'éxécution.
  ```bash
  wget -q --show-progress https://git.mylloon.fr/Anri/downloadcodium/raw/branch/main/updateVSCodium.sh -O tmp.sh && bash tmp.sh; rm tmp.sh
  ```
  ou
- Téléchargez [`updateVSCodium.sh`](https://git.mylloon.fr/Anri/downloadcodium/raw/branch/main/updateVSCodium.sh) et lancez-le (`bash updateVSCodium.sh`).

Pour lancer depuis `~/.vscodium_scripts` :

- `./VSCodium.AppImage --no-sandbox`

Je recommande de cacher la sortie de Codium et de le désaffecter du terminal dans lequel il a été lancé en faisant ceci au lieu de la commande au dessus :

- `./VSCodium.AppImage --no-sandbox > /dev/null 2>&1 & disown`

## Utilité

Script pour télécharger codium version portable en une ligne :

- mettre à jour sans devoir aller sur github à chaque fois
- ferme et/ou remplace automatiquement codium au lancement si ouvert et/ou présent
- commandes simples ajouté à bash, zsh et fish pour permettre d'utiliser et de mettre-à-jour Codium.

A l'avenir pour mettre à jour Codium, lancez `updateCodium`.

## Avoir des extensions utile même avec Codium

Pour télécharger le `.vsix` rendez-vous sur le [magasin de VS Code](https://marketplace.visualstudio.com/) et dans la catégorie `Resources` à droite cliquez sur `Download Extension`.

Pour l'installer dans VS Code allez dans l'onglet `Extensions` (Ctrl + Maj + X), les 3 petits points en haut a droite du petit menu qui viens de s'ouvrir, `Installez à partir de VSIX...`.

Déjà quelques extensions sympa pour éviter de la chercher soi-même :

- Pour lire des PDF : [vscode-pdf](https://marketplace.visualstudio.com/items?itemName=tomoki1207.pdf)
- Pour le language C : [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

# Désinstallation

```bash
rm -r $HOME/.vscodium_scripts
```

Vous devez aussi retirer les alias ajoutés aux shell dans les fichiers suivant : `.bashrc` et `.zshrc`. C'est les lignes où la première ligne commence par:

```bash
# Alias for Codium
```

Pour les utilisateurs de Fish, vous n'avez pas de lignes à supprimer mais des commandes à entrer :

```bash
abbr -e updateCodium code
```

### A Paris 8

A la fac, pour rajouter un raccourci dans le dock en bas de l'écran, vous pouvez :

- soit le faire avec la GUI en suivant ces étapes :
  - Clic-droit sur le dock -> `Tableau de bord` -> `Ajouter de nouveaux éléments`
  - Choisissez l'option `Lanceur` et cliquez sur `Ajouter`, vous pouvez ensuite fermer la fenêtre
  - Clic-droit sur la nouvelle icône qui a été rajouté au dock -> `Propriété`
  - Dans l'onglet Général, cliquez sur le bouton "+" à droite
  - Choissiez une application au hasard
  - Ensuite, modifiez cette application en choisisant l'icône crayon à droite du menu de droite
  - Changez le nom en "`VSCodium`", retirez le commentaire et mettez cette commande : `/home/VOUS/.vscodium_scripts/VSCodium.AppImage --no-sandbox %F` (en remplaçant "VOUS" par votre login de session)
  - Vous pouvez également changer l'icône du raccourci
  - Décochez les deux options disponible et cliquez sur `Enregistrer`
- soit ajouter un dossier `launcher-x` (ou `x` est le le chiffre le plus grand en comparatif aux autres dossiers +1) dans `$HOME/.config/xfce4/panel` qui contient quelque chose similaire à ceci (en remplaçant "VOUS" par votre login de session) :

```bash
[Desktop Entry]
Name=VSCodium
Comment=Code Editing. Redefined.
GenericName=Text Editor
Exec=/home/VOUS/.vscodium_scripts/VSCodium.AppImage --no-sandbox %F
Icon=vscodium
Type=Application
StartupNotify=false
StartupWMClass=VSCodium
Categories=Utility;TextEditor;Development;IDE;
MimeType=text/plain;inode/directory;
Actions=new-empty-window;
Keywords=vscode;
X-Desktop-File-Install-Version=0.23
X-XFCE-Source=file:///usr/share/applications/codium.desktop

Path=
Terminal=false

[Desktop Action new-empty-window]
Name=New Empty Window
Exec=/home/VOUS/.vscodium_scripts/VSCodium.AppImage --no-sandbox --new-window %F
Icon=vscodium
```

Vous n'aurez pas d'icone sur un pc n'ayant pas VSCodium d'installé par défaut mais le raccourci fonctionnera quand même.
