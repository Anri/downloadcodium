#!/usr/bin/env bash

# The goal is :
# -> to create a folder ".vscodium_scripts"
#    at $HOME who have this script and the .AppImage
#
# -> to create some shortcut "code" for codium
#    and "updateCodium" to relaunch this script
#    and update codium

# kill codium if already running
if pgrep -f "codium" > /dev/null
then
    pkill -9 codium
fi

if [ -x "$HOME"/.vscodium_scripts ] ; then # if the folder already exists (not the first run?)
    # go to the folder and clear it
    cd "$HOME/.vscodium_scripts" || exit
    rm ./*

else # if the folder isn't already created (first run?)
    # creating the folder
    mkdir "$HOME/".vscodium_scripts
    cd "$HOME"/.vscodium_scripts || exit

    echo ""
    echo "First run, adding commmands to shell, you will need to restart your shell"
    echo "to use theses commands : \`code\` for Codium and \`updateCodium\` to update it."

    # adding code command and updateCodium command
    # to all the shell installed

    # to .bashrc (i assume it always exists, anyways if not this script is hard to start
    # because all the alias/abbr are using bash)
    {
        echo ""
        echo "# Alias for Codium"
        echo "alias updateCodium='bash \$HOME/.vscodium_scripts/updateVSCodium.sh'"
        echo "alias code='\$HOME/.vscodium_scripts/VSCodium.AppImage --no-sandbox > /dev/null 2>&1 & disown'"
        echo ""
    } >> "$HOME"/.bashrc

    # to .zshrc if exists
    if [ -f "$HOME"/.zshrc ] ; then
        {
            echo ""
            echo "# Alias for Codium"
            echo "alias updateCodium='bash \$HOME/.vscodium_scripts/updateVSCodium.sh'"
            echo "alias code='\$HOME/.vscodium_scripts/VSCodium.AppImage --no-sandbox > /dev/null 2>&1 & disown'"
            echo ""
        } >> "$HOME"/.zshrc
    fi

    # if config.fish exists
    if [ -f "$HOME"/.config/fish/config.fish ] ; then
        fish -c "abbr updateCodium 'bash \$HOME/.vscodium_scripts/updateVSCodium.sh'"
        fish -c "abbr code 'bash \$HOME/.vscodium_scripts/VSCodium.AppImage --no-sandbox > /dev/null 2>&1 & disown'"
    fi
fi

# downloading this script in the folder
wget -q --show-progress https://git.mylloon.fr/Anri/downloadcodium/raw/branch/main/updateVSCodium.sh

# retrieve latest vscodium package url
wget -q https://api.github.com/repos/VSCodium/vscodium/releases/latest
grep 'browser_download_url": ".*.AppImage"' latest | awk '{ print substr ($0, 32 ) }' | awk '{ print substr( $0, 1, length($0)-1 ) }' > url
rm latest

# download the latest vscodium AppImage
wget -q --show-progress -i url -O VSCodium.AppImage
rm url

# make it executable
chmod u+x VSCodium.AppImage


echo ""
# not really installed because no packet manager know what happened
echo "VSCodium is now downloaded and \"installed*\" at $PWD/VSCodium.AppImage."
#echo "You need to restart your shell to use all the new commands."
echo "\`updateCodium\` -> update Codium to the latest version"
echo "\`code\` -> start Codium"
